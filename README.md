# Elogic Wishlist module

> Add products to or remove from wishlist via AJAX

## Description

AJAX wishlist extension allows to add a product to wishlist and then remove it from it without page reload. If user is not logged in he/she will be redirected to the login page. It make wishlist functionality user friendly and more simple

## Installation

1. Go to Magento2 root folder
2. Require this extension, enter following command to install extension: ```composer require elogic/module-wishlist```
3. Wait while composer is updated.

### OR

You can also download code from our repo:
https://gitlab.com/elogic-group/elogic-wishlist

Enter following commands to enable the module:

```
php bin/magento module:enable Elogic_Wishlist
php bin/magento setup:upgrade
```


## Requirements

Magento installation


## Compatibility

Extension is created for both Magento- and Breeze-based themes.
Default version works for default Magento themes, you can remove all sample files if extension installed in app/code folder.

To make it work with the Breeze-based themes:

1. Copy and paste the contents of the following files (to app/code/Elogic_Wishlist):
- ```components/ajax-wishlist.js.sample``` to ```components/ajax-wishlist.js```
- ```components/ajax-wishlist.js.sample``` to ```components/ajax-wishlist.js```
- ```checkout-components/ajax-wishlist.js.sample``` to ```checkout-components/ajax-wishlist.js```
- ```requirejs-config.js.sample``` to ```requirejs-config.js```
- ```layout/default.xml.sample``` to ```layout.default.xml```


## TODO list

1. ~~Override PB grid/carousel templates~~
2. ~~Update extension instructions~~
3. Make it work for cart page
