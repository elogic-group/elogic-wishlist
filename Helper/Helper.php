<?php

declare(strict_types=1);

namespace Elogic\Wishlist\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Helper extends AbstractHelper
{
    const AJAX_WISHLIST_CONFIG_PATH = 'ajax_wishlist/general/enable_ajax_wishlist';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param Context $context
     * @param \Magento\Wishlist\Model\Wishlist $wishlist
     * @param \Magento\Customer\Model\Session $customer
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        \Magento\Wishlist\Model\Wishlist $wishlist,
        \Magento\Customer\Model\Session $customer,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->wishlist = $wishlist;
        $this->customer = $customer;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Retrieve true if product is in customer`s wishlist
     * Retrieve false if it is not
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param int|null $customerId
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isItemInWishlist(\Magento\Catalog\Api\Data\ProductInterface $product, int $customerId = null) : bool
    {
        if (is_null($customerId)) {
            $customerId = $this->customer->getId();
        }

        $wishlistCollection = $this->wishlist->loadByCustomerId($customerId, true)->getItemCollection();

        try {
            foreach ($wishlistCollection as $item) {
                if ($product->getId() == $item->getProduct()->getId()) {
                    // current item is added in wishlist
                    return true;
                } else {
                    // current item not added in wishlist
                    continue;
                }
            }
        } catch (\Exception $exception) {
            //just return false
        }
        return false;
    }

    /**
     * Get is AJAX Wishlist enabled
     */
    public function isAjaxWishlistEnabled() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::AJAX_WISHLIST_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }
}
