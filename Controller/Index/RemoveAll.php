<?php

declare(strict_types=1);

namespace Elogic\Wishlist\Controller\Index;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Framework\App\Request\Http as Request;
use Magento\Wishlist\Model\ResourceModel\Wishlist as WishlistResourceModel;

class RemoveAll implements HttpPostActionInterface
{
    /**
     * @param Validator $validator
     * @param WishlistProviderInterface $wishlistProvider
     * @param StoreManagerInterface $storeManager
     * @param ResultFactory $resultFactory
     * @param Request $request
     * @param WishlistResourceModel $wishlistResourceModel
     */
    public function __construct(
        Validator $validator,
        WishlistProviderInterface $wishlistProvider,
        StoreManagerInterface $storeManager,
        ResultFactory $resultFactory,
        Request $request,
        WishlistResourceModel $wishlistResourceModel
    ) {
        $this->wishlistProvider = $wishlistProvider;
        $this->formKeyValidator = $validator;
        $this->storeManager = $storeManager;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->wishlistResourceModel = $wishlistResourceModel;
    }

    /**
     * Execute action based on request and return result
     *
     * @return Json
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function execute(): \Magento\Framework\Controller\Result\Json
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setJsonData('{"url":"' . $this->storeManager->getStore()->getBaseUrl() . 'wishlist"   }');

        if (!$this->formKeyValidator->validate($this->request)) {
            $result->setHttpResponseCode(412);
            return $result;
        }

        /** @var \Magento\Wishlist\Model\Wishlist $wishlist */
        $wishlist = $this->wishlistProvider->getWishlist();
        /** @var \Magento\Wishlist\Model\ResourceModel\Item\Collection $wishlistCollection */
        $wishlistCollection = $wishlist->getItemCollection();
        foreach ($wishlistCollection as $item) {
            $item->delete();
        }
        $this->wishlistResourceModel->save($wishlist);

        $result->setHttpResponseCode(200);
        return $result;
    }
}
