<?php

declare(strict_types=1);

namespace Elogic\Wishlist\Controller\Index;

use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\App\Request\Http as Request;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Wishlist\Model\ResourceModel\Item as ItemResourceModel;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Wishlist\Model\Wishlist;

class RemoveItem implements HttpPostActionInterface
{
    /**
     * @param Wishlist $wishlist
     * @param Validator $validator
     * @param Request $request
     * @param Session $session
     * @param ResultFactory $resultFactory
     * @param ItemResourceModel $itemResourceModel
     */
    public function __construct(
        \Magento\Wishlist\Model\Wishlist $wishlist,
        Validator                        $validator,
        Request                          $request,
        Session                          $session,
        ResultFactory                    $resultFactory,
        ItemResourceModel                $itemResourceModel
    ) {
        $this->wishlist = $wishlist;
        $this->formKeyValidator = $validator;
        $this->request = $request;
        $this->session = $session;
        $this->resultFactory = $resultFactory;
        $this->itemResourceModel = $itemResourceModel;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     * @throws NoSuchEntityException
     */
    public function execute(): \Magento\Framework\Controller\Result\Json
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if (!$this->formKeyValidator->validate($this->request)) {
            $result->setHttpResponseCode(412);
            return $result;
        }

        $customerId = $this->session->getCustomerId();
        $wish = $this->wishlist->loadByCustomerId($customerId);
        $items = $wish->getItemCollection();

        $productId = $this->request->getParam('product_id');

        /** @var \Magento\Wishlist\Model\Item $item */
        foreach ($items as $item) {
            if ($item->getProductId() == $productId) {
                $this->itemResourceModel->delete($item);
            }
        }
        $result->setHttpResponseCode(200);
        return $result;
    }
}
