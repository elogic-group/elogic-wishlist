define([
    'jquery',
    'underscore',
    'mage/template',
    'Magento_Ui/js/modal/confirm'
], function (
    $,
    _,
    template,
    confirmation
) {
    function main(config, element) {
        $(document).on('click', '.button-wishlist-remove-all-items', function (e) {
            e.preventDefault();

            confirmation({
                title: $.mage.__('Are you sure?'),
                content: $.mage.__('Do you really want to delete all items? This process cannot be undone.'),
                modalClass: 'delete-confirm',
                actions: {
                    confirm: function () {
                        var AjaxUrl = config.AjaxUrl;
                        var formKey = $.mage.cookies.get('form_key');
                        $.ajax({
                            showLoader: true,
                            url: AjaxUrl,
                            data: {
                                'form_key': formKey
                            },
                            type: "POST",
                            dataType: 'json',
                            success: function (response) {
                                window.location = response.url;
                            }
                        });
                    },
                },
                buttons: [{
                    text: $.mage.__('Delete'),
                    class: 'action-secondary action-accept',
                    click: function (event) {
                        this.closeModal(event, true);
                    }
                }, {
                    text: $.mage.__('Cancel'),
                    class: 'action-primary action-dismiss',
                    click: function (event) {
                        this.closeModal(event);
                    }
                }]
            });
        });
    }

    return main;
});
