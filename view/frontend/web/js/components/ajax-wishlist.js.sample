/**
 * Elogic Wishlist - Ajax wishlist
 * Adds functionality to check if current product is added to the wishlist,
 * add/remove product to the wishlist via ajax
 */

$.view('ajaxWishlist', {
    component: 'Elogic_Wishlist/js/components/ajax-wishlist',
    wishlist: $.sections.get('wishlist'),
    customer: $.sections.get('customer'),
    wishlistPostParams:  ko.observable(''),

    /**
     * Init component
     */
    create: function() {
        var wishlist = this.wishlist(),
            wishlistItems = wishlist['items'];


        if (wishlistItems !== undefined && wishlistItems !== null) {
            this.wishlistPostParams(this.options.wishlistParams);

            var isProductInWishlist = false;

            wishlistItems.forEach((item) => {
                if (parseInt(item['product_id']) === this.currentProductId) {
                    isProductInWishlist =  true;
                }
            });
        }
    },

    /**
     * Check if product is in wishlist
     *
     * @param currentProductId
     * @returns {boolean}
     */
    isInWishlist: function(currentProductId) {
        var wishlist = this.wishlist(),
            wishlistItems = wishlist['items'],
            result = false;

        if (wishlistItems !== undefined && wishlistItems !== null) {
            this.wishlistPostParams(this.options.wishlistParams);

            wishlistItems.forEach((item) => {
                if (parseInt(item['product_id']) === currentProductId) {
                    result = true;
                }
            });
        }
        return result;
    },

    /**
     * Ajax add to wishlist
     */
    ajaxAddToWishlist: function() {
        var customer =  this.customer(),
            addEndpoint = '/wishlist/index/add',
            url = '/customer/account/login';

        if (customer.firstname) {
            this.sendAjaxRequest(addEndpoint);
        } else {
            window.location.href = url;
        }
    },

    /**
     * Ajax remove from wishlist
     */
    ajaxRemoveFromWishlist: function() {
        var removeEndpoint = '/wishlist/index/removeItem';

        this.sendAjaxRequest(removeEndpoint);
    },

    /**
     * Send ajax request
     *
     * @param url
     */
    sendAjaxRequest: function (url) {
        var formKey = $.cookies.get('form_key'),
            itemId = this.options.currentProductId,
            itemSelector = $(this.element).closest('.product-item');

        $(itemSelector).spinner(true);

        $.request.post({
            url: url,
            data: {
                'form_key': formKey,
                'product_id': itemId
            },
            dataType: 'json',

            complete: function() {
                $.sections.reload(['wishlist']);

                $(itemSelector).spinner(false);
            }
        });
    }
});
