/**
 * Elogic - AJAX Wishlist
 * Addded function to check if current product is added to the wishlist, add product to the wishlist via ajax
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'mage/cookies',
    'loader'
], function ($, ko, Component, customerData) {
    'use strict';

    return Component.extend({
        defaults: {
            component: 'Elogic_Wishlist/js/components/ajax-wishlist',
            showWishlist:  ko.observable(false),
            wishlistPostParams:  ko.observable(''),
            customer: customerData.get('customer'),
            wishlist: customerData.get('wishlist')
        },

        /**
         * Initialize function
         */
        initialize: function () {
            this._super();

            var wishlistItems = this.wishlist()['items'];

            if (wishlistItems !== undefined && wishlistItems !== null) {

                var isProductInWishlist = false;

                wishlistItems.forEach((item) => {
                    if (parseInt(item['product_id']) === this.currentProductId) {
                        isProductInWishlist =  true;
                    }
                });
            }
        },

        /**
         * Check if product is in wishlist
         *
         * @param currentProductId
         * @returns {boolean}
         */
        isInWishlist: function (currentProductId) {
            var wishlistItems = this.wishlist()['items'],
                result = false;

            if (wishlistItems !== undefined && wishlistItems !== null) {
                this.wishlistPostParams(this.wishlistParams);
                wishlistItems.forEach((item) => {
                    if (parseInt(item['product_id']) === currentProductId) {
                        result = true;
                    }
                });
            }
            return result;
        },

        /**
         * Ajax add to wishlist
         */
        ajaxAddToWishlist: function() {
            var addEndpoint = '/wishlist/index/add',
                url = '/customer/account/login';

            if (this.customer().firstname) {
                this.sendAjaxRequest(addEndpoint);
            } else {
                window.location.href = url;
            }
        },

        /**
         * Ajax remove from wishlist
         */
        ajaxRemoveFromWishlist: function() {
            var removeEndpoint = '/wishlist/index/removeItem';

            this.sendAjaxRequest(removeEndpoint);
        },

        /**
         * Send ajax request
         *
         * @param url
         */
        sendAjaxRequest: function (url) {
            var formKey = $.mage.cookies.get('form_key');

            $('body').loader('show');

            $.ajax({
                url: url,
                type: 'post',
                data: {
                    'form_key': formKey,
                    'product_id': this.currentProductId
                },
                dataType: 'json',

                complete: function() {
                    customerData.reload(['wishlist']);

                    $('body').loader('hide');
                }
            });
        }
    });
});
