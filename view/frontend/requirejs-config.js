/**
 * Cart/Checkout - JS config
 */

var config = {
    map: {
        '*': {
            'ajaxWishlist': 'Elogic_Wishlist/js/components/ajax-wishlist',
            'wishlistRemoveAll': 'Elogic_Wishlist/js/components/remove-all'
        }
    }
};
